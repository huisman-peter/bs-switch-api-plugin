var glob = require("glob");
var fs = require('fs');
var apiServer = require('browser-sync').create('apiServer');
var PLUGIN_NAME = 'API Scenario switch';
var enabled = true;
var currentScenario = './api/default.json';

var os = require('os');
var ifaces = os.networkInterfaces();



apiServer.init({
  cors: true,
  open: false,
  ui: false,
  port: 9999,
  server: './api',
  middleware: [
    function (req, res, next) {
      var apiData = JSON.parse(fs.readFileSync(currentScenario).toString());
      var route = apiData.routes.filter(function (route) {
        if (route[req.url]) {
          if (route[req.url][req.method]) {
            return route;
          }
        }
      })[0];
      if (route) {
        var routeData = route[req.url][req.method].response;
        var resHeaders = {};
        resHeaders['Content-Type'] = 'application/json';
        if (routeData.headers && typeof routeData.headers.forEach) {
          routeData.headers.forEach(function (hdr) {
            Object.keys(hdr).forEach(function (key) {
              resHeaders[key] = hdr[key];
            });
          });
        }
        res.writeHead(routeData.status, resHeaders);
        res.end(JSON.stringify(routeData.body));
      } else {
        next();
      }
    }
  ]
});


module.exports = {

  plugin: function (opts, bs) {

    var logger = bs.getLogger(PLUGIN_NAME).info("Running...");
    var clients = bs.io.of(bs.options.getIn(["socket", "namespace"]));

    // handle socket events
    handleSocketConnection = function (socket) {
      // return list of scenario's'
      socket.on("scenarios:request", function (data) {
        if (!enabled) {
          return;
        }
        glob("./api/**/*.json", function (err, files) {
          var scenarios = [];
          files.forEach(function (f) {
            var selected = false;
            if (f === currentScenario) {
              selected = true;
            }
            scenarios.push(
              {
                file: f,
                selected: selected
              }
            );
          });
          socket.emit('scenarios:response', { 'scenarios': scenarios });
        });
      });

      // set current Scenario
      socket.on("scenario:select", function (data) {
        if (!enabled) {
          return;
        }
        currentScenario = data.scenario;
        clients.emit("scenario:changed", currentScenario);
      });

    };

    // handle socket connection on connection
    clients.on("connection", handleSocketConnection);

    // enable or disable plugin
    bs.events.on("plugins:configure", function (data) {
      if (data.name !== PLUGIN_NAME) {
        return;
      }
      var msg = PLUGIN_NAME + " enabled";
      if (!data.active) {
        msg = PLUGIN_NAME + " disabled";
      } else {
        clients.emit("browser:reload");
      }
      logger.info(msg);
      enabled = data.active;
    });
  },
  'plugin:name': PLUGIN_NAME,
  hooks: {
    'client:js': require('fs').readFileSync(__dirname + '/client.js', 'utf8')
  }
}
