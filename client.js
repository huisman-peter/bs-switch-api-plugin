(function (bs) {
    
    var socket = bs.socket;
    
    var createUI = function(data){
        window.___pggm_api___ = document.createElement('select');
        ___pggm_api___.setAttribute('style', 
            'position:fixed; right:20px; bottom:20px; z-index: 999999; visibility: hidden');
            
        document.body.appendChild(window.___pggm_api___);
        var optionhtml = '';
        data.scenarios.forEach(function(item){
            var selatt = '';
            if(item.selected){
                selatt = ' selected ';
            }
            optionhtml = optionhtml + '<option' +selatt + ' value="'+ item.file +'">' + item.file + '</option>';
        });
        ___pggm_api___.innerHTML = optionhtml;
        ___pggm_api___.addEventListener('change', function(e){
            socket.emit('scenario:select', {'scenario': ___pggm_api___.options[___pggm_api___.selectedIndex].text });
        });
    };

    socket.on("connection", function () {
        socket.emit("scenarios:request", {});
        window.addEventListener('keydown', function(e){
            if(___pggm_api___){
                if(e.keyCode === 65 && e.ctrlKey && e.shiftKey){
                
                    if(___pggm_api___.style.visibility === 'visible'){
                        ___pggm_api___.style.visibility = 'hidden';
                    } else{
                        ___pggm_api___.style.visibility = 'visible';
                    }
                
                }
            }
        });
    });

    socket.on("scenarios:response", function (data) {
        createUI(data);
    });
    socket.on("scenario:changed", function (data) {
        ___pggm_api___.value = data
    });

})(window.___browserSync___);